from django.contrib import admin

from .models import Banner, Brand

admin.site.register(Banner)

admin.site.register(Brand)
