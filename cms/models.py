from django.db import models
from sorl.thumbnail import ImageField
from ckeditor.fields import RichTextField
from autoslug import AutoSlugField  # no need to add autoslug


class Banner(models.Model):
    caption = models.CharField(max_length=70)
    image = ImageField()
    weight = models.IntegerField()
    published = models.BooleanField()

    def __str__(self):
        return str(self.caption)


class Brand(models.Model):
    title = models.CharField(max_length=20)
    image = ImageField()

    def __str__(self):
        return self.title
