from django.urls import path
from .views import *

app_name = 'ecommerce'  # namespace

urlpatterns = [
    path('', Homepage.as_view(), name='index'),
    path('register', SignUpView.as_view(), name='register'),
    path('category/<str:category_slug>', CategoryWise.as_view(), name='category_wise'),
    path('product/<str:product_slug>', ProductView.as_view(), name='single_product'),
    path('search/', SearchView.as_view(), name='search'),
    path('api/categories', CategoryApiView.as_view()),
    path('cart', CartView.as_view(), name='cart'),
    path('demo/<int:id>', demoGlass.as_view(), name='demoGlass'),
    path('remove/<int:id>', remove_product, name='remove_product'),
    path('contactus', ContactUsView.as_view(), name='ContactUs'),
    path('orderplacement', OrderPlacementView.as_view(), name='Order_placement')

]
