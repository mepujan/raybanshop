# Generated by Django 2.1.5 on 2019-05-02 10:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ecommerce', '0011_auto_20190502_1445'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sunglasses',
            name='brand',
        ),
        migrations.RemoveField(
            model_name='sunglasses',
            name='label',
        ),
    ]
