from django.contrib import admin

from ecommerce.models import Category, Product, ProductHasImage, ProductReview, Cart, Sunglasses,ContactUs


class ProductHasImageInline(admin.TabularInline):
    model = ProductHasImage


class AdminProduct(admin.ModelAdmin):
    inlines = [ProductHasImageInline]
    search_fields = ('title', 'description')


admin.site.register(Category)

admin.site.register(Product, AdminProduct)

admin.site.register(ProductReview)
admin.site.register(ProductHasImage)

#

admin.site.register(Cart)
admin.site.register(Sunglasses)
admin.site.register(ContactUs)
