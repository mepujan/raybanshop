from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from django.forms import ModelForm
from django import forms
from ecommerce.models import ProductReview, ContactUs, Orders


class SignUpForm(UserCreationForm):
    street = forms.CharField(max_length=50)
    city = forms.CharField(max_length=50)
    DOB = forms.DateField(widget=forms.TextInput(attrs={'placeholder': 'Format: MM/dd/yyyy'}))
    # gender = forms.ChoiceField(widget=forms.RadioSelect, choices=('Male', 'Female', 'Others'))

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'DOB', 'street',
                  'city']


class CommentForm(ModelForm):
    class Meta:
        model = ProductReview
        fields = ('rating', 'comment')


class ContactUsForm(ModelForm):
    class Meta:
        model = ContactUs
        fields = ['email', 'subject', 'message']


class ConfirmProductForm(ModelForm):
    class Meta:
        model = Orders
        fields = ['current_contact', 'current_address']
